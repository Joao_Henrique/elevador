#!/bin/sh

cd BME280_driver/examples/contributed/

make clean
make raspbian
chmod +x bme280

cd ../../../

mv BME280_driver/examples/contributed/bme280 elevador/bin/bme280

cd elevador/

make clean
make

cd bin/

./prog
