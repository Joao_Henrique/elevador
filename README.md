# Elevador

![Elevador generico](https://s2.glbimg.com/LH1jFZ-nHyN3woCvi8WfSruNT50=/620x455/e.glbimg.com/og/ed/f/original/2021/08/26/elevador-mito-duvida-verdade-sistema-funcionamento.jpg)


Este projeto é o segundo trabalho da matéria **Fundamentos de Sistemas Embarcados**, a descrição do projeto encontra-se
dentro da pasta [descricao_trabalho](https://gitlab.com/fse_fga/trabalhos-2023_2/trabalho-2-2023-2).

[VIDEO](https://youtu.be/mpjWh9cYv8s)
## Organização do repositório

### Driver BME280

O código deste repositório é específico para a rasp40, que possui o sensor BME280.
O único arquivo alterado deste driver foi o [linux_userspace.c](BME280_driver/examples/contributed/linux_userspace.c),
o intuito foi de fazer com que o binário gerado deste arquivo retornasse apenas uma string com os seguintes valores:

```c
printf("%0.2lf", temp);
```

### elevador

É onde o código principal está localizado. Suas pastas são src/ inc/ obj/ bin/.
A comunicação do código principal com o Driver BME280 é feita através do acesso
direto ao binário gerado com o [Makefile](BME280_driver/examples/contributed/Makefile),
este binário é gerado e movido para dentro de bin/ automaticamente pelo script [compile.sh](compile.sh).

#### Contato entre código principal e código da bme280.

[get_temperature](elevador/src/get_data_for_lcd.c)

## Como executar o código

```bash
chmod +x compile.sh
./compile.sh
```
