#ifndef GPIO_CONFIG
#define GPIO_CONFIG

extern const int DIR1;
extern const int DIR2;
extern const int POTM;

extern const int SENSOR_TERREO;
extern const int SENSOR_1_ANDAR;
extern const int SENSOR_2_ANDAR;
extern const int SENSOR_3_ANDAR;

int set_up_gpio();

#endif

