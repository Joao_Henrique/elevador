#ifndef UART_COMMUNICATION
#define UART_COMMUNICATION

int init_uart();
void close_uart();

int read_encoder_uart(int* encoder_value);
int write_temperature_uart(char* temperature_string);
int write_pwm_uart(int* pwm_control_signal);

int read_all_registers_uart(char* buttons_buffer, char max_elements);
int write_all_registers_uart(char* buttons_buffer, char max_elements);

#endif
