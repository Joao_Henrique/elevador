#ifndef ELEVATOR_H_
#define ELEVATOR_H_

extern const int FREE;
extern const int DOWN;
extern const int UP;
extern const int BRIDLE;

extern int position_floor[4];

void elevator_go_to_position(int position_to_go, int* action, int* potency);
void elevator_calibration(int* action, int* potm);


#endif
