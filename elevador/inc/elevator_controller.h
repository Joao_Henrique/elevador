#ifndef ELEVATOR_CONTROLLER
#define ELEVATOR_CONTROLLER

typedef struct address_button{
  char btn_up;
  char btn_down;
  char btn_elevator;
} AddressButton;

extern AddressButton address_button_floor[4];

void init_elevator_controller(int* potm, int* action);
void close_elevator_controller();

int seek_btn_pressed_then_go_forward_elevator_controller();

#endif
