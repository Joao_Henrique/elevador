#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include "get_data_for_lcd.h"
#include "lcd_write.h"

#include "elevator.h"
#include "elevator_controller.h"
#include "uart_communication.h"

typedef struct lcd_uart_update_info_params {
	int status_elevator;
	int potm_elevator;
} LcdUartUpdateInfoParams;

void* lcd_uart_update_info(LcdUartUpdateInfoParams* params);

int main()
{
  pthread_t thread_lcd_uart_update_info;

  LcdUartUpdateInfoParams lcd_uart_params = {FREE, 19};

	printf("MAIN\n");
  pthread_create(&thread_lcd_uart_update_info, NULL, lcd_uart_update_info, (LcdUartUpdateInfoParams*) &lcd_uart_params);

 	printf("ELEVATOR CALIBRATION\n");
  elevator_calibration(&lcd_uart_params.status_elevator, &lcd_uart_params.potm_elevator); 

	init_elevator_controller(&lcd_uart_params.status_elevator, &lcd_uart_params.potm_elevator);
	while(1)
		{ seek_btn_pressed_then_go_forward_elevator_controller(); sleep(1); }

	close_elevator_controller();

	pthread_join(thread_lcd_uart_update_info, NULL);

	return 0;
}

void* lcd_uart_update_info(LcdUartUpdateInfoParams* params)
{
	char temperature_string[50];
	char status_string[50];

	while(! init_uart() )sleep(1);

	while(1)
	{
		get_temperature( temperature_string );
		get_string_from_status_elevator( params->status_elevator, status_string );

		lcd_write_messages( temperature_string, status_string );
		sleep(1);

		if(!write_temperature_uart(temperature_string))
			printf("ERROR TEMP UART\n");
		if(!write_pwm_uart(&params->potm_elevator))
			printf("ERROR PWM UART\n");
	}
	close_uart();

  pthread_exit(NULL);
}
