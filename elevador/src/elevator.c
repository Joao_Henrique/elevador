#include <stdio.h>
#include <unistd.h>
#include <wiringPi.h>
#include <softPwm.h>

#include "elevator.h"

#include "gpio_config.h"
#include "uart_communication.h"
#include "pid.h"

int absolute(int value)
  { value = (value < 0) ? (value) * -1 : value; return value;}


int position_floor[4];

const int FREE = 0;
const int DOWN = 1;
const int UP = 2;
const int BRIDLE = 3;

/*
*
* Action |Dir1| Dir2 | INT_VALUE
* Free   | 0  | 0    |    0
* Down   | 0  | 1    |    1
* Up     | 1  | 0    |    2
* Bridle | 1  | 1    |    3
*
*/

void elevator_set_action(int action) 
{
  // printf("ACTION %d\n", action);
  if (!(action == FREE || action == DOWN
      ||action == UP || action == BRIDLE ))
    return;

  // Converting int to binary notation to set gpio to high or low.
  int direction1, direction2;
  direction1 = (action & 2) ? HIGH : LOW;
  direction2 = (action & 1) ? HIGH : LOW;

  // printf("%d %d\n", direction1, direction2);

  digitalWrite(DIR1, direction1);
  digitalWrite(DIR2, direction2);
}

int find_floor(int floor_gpio_number)
{
  int position_floor = 0;
  int found_floor = LOW;

	while(found_floor != HIGH){
	  found_floor = digitalRead(floor_gpio_number);
	  delay(10);
	}
  while(!read_encoder_uart(&position_floor));

  return position_floor;
}

void elevator_go_to_position(int position_to_go, int* action, int* potency)
{
	while(!( set_up_gpio() && init_uart() ) )
	  sleep(1);

  int my_positon;
  while(!read_encoder_uart(&my_positon));

  pid_configura_constantes(0.5, 0.05, 40.0);
  pid_atualiza_referencia(position_to_go);

  *action = (my_positon > position_to_go) ? DOWN : UP;

  if( position_to_go == my_positon || my_positon < 0 || position_to_go < 0)
    return;

  elevator_set_action(*action);

	int delta = absolute(position_to_go - my_positon);
	while(delta >= 40)
  {
    while(!read_encoder_uart(&my_positon));
	  *potency = (int) pid_controle(my_positon);
	  *potency = absolute(*potency);

	  softPwmWrite(POTM, *potency);

	  // printf("_______________________\n");
	  delta= absolute(position_to_go - my_positon);
	  // printf("\t(GO,MY): (%d,%d)\n", position_to_go, my_positon);
	  // printf("\t(DELTA) -> |%d|\n", delta);

    if( delta <= 220)
    {
      *action = FREE;
      elevator_set_action(*action);
    }
	  usleep(150000);
  }
	*potency = 0;
	*action = BRIDLE;

	softPwmWrite(POTM, *potency);
  elevator_set_action(*action);

	printf("_______________________\n");
	delta= absolute(position_to_go - my_positon);
	printf("\t(GO,MY): (%d,%d)\n", position_to_go, my_positon);
	printf("\t(DELTA) -> |%d|\n", delta);

}

void elevator_calibration(int* action, int* potm)
{
	while(!( set_up_gpio() && init_uart() ) )
	  printf("Calibration elevator error, re trying connection with gpio and uart.\n");

  *potm = 100;
  // Reset elevator.
  elevator_go_to_position(0,potm, action);

  // Set to go up slowly.
  *action = UP;
  elevator_set_action(*action);
  *potm = 50;
  softPwmWrite(POTM, *potm);

  // Capture positions.
  position_floor[0] = find_floor(SENSOR_TERREO);
  position_floor[1] = find_floor(SENSOR_1_ANDAR);
  position_floor[2] = find_floor(SENSOR_2_ANDAR);
  position_floor[3] = find_floor(SENSOR_3_ANDAR);

  printf("Floor: 0 -> (%d) 1 -> (%d) 2 -> (%d) 3 -> (%d)\n",
        position_floor[0], position_floor[1],
        position_floor[2], position_floor[3]);

  // Get positions on way down.
  *action = DOWN;
  elevator_set_action(*action);

  // Capture positions again.
  position_floor[3] = (find_floor(SENSOR_3_ANDAR) + position_floor[3]) / 2;
  position_floor[2] = (find_floor(SENSOR_2_ANDAR) + position_floor[2]) / 2;
  position_floor[1] = (find_floor(SENSOR_1_ANDAR) + position_floor[1]) / 2;
  position_floor[0] = (find_floor(SENSOR_TERREO)  + position_floor[0]) / 2;

  printf("Floor: 0 -> (%d) 1 -> (%d) 2 -> (%d) 3 -> (%d)\n",
        position_floor[0], position_floor[1],
        position_floor[2], position_floor[3]);
}
