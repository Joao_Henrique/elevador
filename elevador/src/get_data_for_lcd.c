#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "elevator.h"
#include "get_data_for_lcd.h"

void get_temperature(char* message) {
    const char* command = "./bme280";

    FILE* pipe = popen(command, "r");
    if (!pipe) {
        perror("popen failed");
        return;
    }

    char buffer[128];

    while (fgets(buffer, sizeof(buffer), pipe) != NULL);

    pclose(pipe);

    strncpy(message,buffer, sizeof(buffer));
}

void get_string_from_status_elevator(int status_code, char* message) {
    char status_string[15] ;

    if ( status_code == FREE)
        strncpy(status_string,"Livre", sizeof("Livre"));
    else if ( status_code == DOWN)
        strncpy(status_string,"Descendo", sizeof("Descendo"));
    else if ( status_code == UP)
        strncpy(status_string,"Subindo", sizeof("Subindo"));
    else if ( status_code == BRIDLE)
        strncpy(status_string,"Parado", sizeof("Parado"));
    else
        strncpy(status_string,"Error code", sizeof("Error code"));

    strncpy(message, status_string, sizeof(status_string));
}
