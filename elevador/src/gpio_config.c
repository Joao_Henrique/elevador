#include <stdio.h>
#include <wiringPi.h>
#include <softPwm.h>

#include "gpio_config.h"

const int DIR1 = 28;
const int DIR2 = 29;
const int POTM = 26;

const int SENSOR_TERREO  = 1;
const int SENSOR_1_ANDAR = 4;
const int SENSOR_2_ANDAR = 5;
const int SENSOR_3_ANDAR = 6;

int gpio_initialized = 0;

int set_up_gpio()
{
  int sucess_set_up_gpio = 0;
  if( gpio_initialized )
    { sucess_set_up_gpio = 1; return sucess_set_up_gpio; }

  if (wiringPiSetup() != -1) {
    sucess_set_up_gpio = 1;
  }


  pinMode(DIR1, OUTPUT);
  pinMode(DIR2, OUTPUT);

  pinMode(SENSOR_TERREO, INPUT);
  pinMode(SENSOR_1_ANDAR, INPUT);
  pinMode(SENSOR_2_ANDAR, INPUT);
  pinMode(SENSOR_3_ANDAR, INPUT);

  pullUpDnControl(SENSOR_TERREO, PUD_DOWN);
  pullUpDnControl(SENSOR_1_ANDAR, PUD_DOWN);
  pullUpDnControl(SENSOR_2_ANDAR, PUD_DOWN);
  pullUpDnControl(SENSOR_3_ANDAR, PUD_DOWN);

	if (softPwmCreate(POTM, 0, 100) == 0 && sucess_set_up_gpio)
	{
    gpio_initialized = 1;
    return sucess_set_up_gpio;
	}

	return !sucess_set_up_gpio;

}
