#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "elevator.h"
#include "elevator_controller.h"

#include "uart_communication.h"

AddressButton address_button_floor[4];

int need_go_to_floor[4];
char buttons_buffer[11];

int controller_inited = 0;

int* self_potm;
int* self_action;

void close_elevator_controller()
  { controller_inited = 0; }

void init_elevator_controller(int* potm, int* action)
{
  if( controller_inited )
    return ;
  // Special cases, for ground and 3. I set btn_up == btn_down to avoid mistakens, with this definition
  // the worst thing that will happen is rewrite this address cause they are pointing to same place.
  address_button_floor[0].btn_up = 0x00;
  address_button_floor[0].btn_down = 0x00;
  address_button_floor[0].btn_elevator = 0x07;

  address_button_floor[3].btn_up = 0x05;
  address_button_floor[3].btn_down = 0x05;
  address_button_floor[3].btn_elevator = 0x0A;

  // Normal cases
  address_button_floor[1].btn_up = 0x02;
  address_button_floor[1].btn_down = 0x01;
  address_button_floor[1].btn_elevator = 0x08;

  address_button_floor[2].btn_up = 0x04;
  address_button_floor[2].btn_down = 0x03;
  address_button_floor[2].btn_elevator = 0x09;

  // Redundant 'force' init zero values.
  for(int i = 0; i < 4; i++)
    need_go_to_floor[i] = 0;

  for(int i = 0; i < 11; i++)
    buttons_buffer[i] = 0;

  // Copy addres to global pointers.
  self_potm = potm;
  self_action = action;

  controller_inited = 1;
	init_uart();
}

int check_need_go_to_floor(int floor_value)
{
  if( floor_value >= 4 || floor_value < 0)
    return 0;

  need_go_to_floor[floor_value] = ( 
      buttons_buffer[address_button_floor[floor_value].btn_up]
    ||buttons_buffer[address_button_floor[floor_value].btn_down]
    ||buttons_buffer[address_button_floor[floor_value].btn_elevator]
  );
  return need_go_to_floor[floor_value];
}
void write_zero_to_buttons_floor(int floor_value)
{
  if( floor_value >= 4 || floor_value < 0)
    return;

  while(! read_all_registers_uart(buttons_buffer, 11))
    { printf("Error read btns\n", floor_value); sleep(1); }

  buttons_buffer[address_button_floor[floor_value].btn_up] = 0;
  buttons_buffer[address_button_floor[floor_value].btn_down] = 0;
  buttons_buffer[address_button_floor[floor_value].btn_elevator] = 0;

  while(! write_all_registers_uart(buttons_buffer, 11))
    { printf("Error write zero to floor: %d\n", floor_value); sleep(1); }
}

int seek_btn_pressed_then_go_forward_elevator_controller()
{
  int sucess_seek_btn_pressed_then_go = 0;
  if( ! controller_inited )
    return sucess_seek_btn_pressed_then_go;

  while(! read_all_registers_uart(buttons_buffer, 11) )
    { printf("Error read buttons_buffer\n"); sleep(1); }

  int i, step, end;
  if( *self_action == UP)
    { i = 0; step = 1; end = 4; }
  else
    { i = 3; step = -1; end = -1; }

  printf("(i, step, end): (%d, %d, %d)\n", i, step, end);
  for(i; i != step; i += step)
  {
    if(check_need_go_to_floor(i))
    {
      printf("NEED TO GO %d\n", i);
      elevator_go_to_position(position_floor[i], self_action, self_potm);
      write_zero_to_buttons_floor(i);
    }
  }

  sucess_seek_btn_pressed_then_go = 1;
  return sucess_seek_btn_pressed_then_go;
}
