#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <semaphore.h>

#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include "uart_communication.h"
#include "crc16.h"

// Buffers

unsigned char tx_buffer[256];
unsigned char rx_buffer[256];

int pointer_tx_buffer;
int pointer_rx_buffer;

// This pointers always are pointing to next buffer
// position.
//               |    -> pointer_(tx/rx)_buffer
// S T R I N G '\0'

// File global vars.
int uart0_filestream = -1;
int uart_is_open = 0;
sem_t mutex;

int init_uart()
{
	int sucess_init_uart = 0;

	// Avoid re-open uart.
	if( uart_is_open )
		{ sucess_init_uart = 1; return sucess_init_uart; }

	uart0_filestream = -1;
	uart0_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);

	if (uart0_filestream == -1)
		printf("Erro - Não foi possível iniciar a UART.\n");
	else
		{ printf("UART inicializada!\n"); sucess_init_uart = 1; }

	struct termios options;
	tcgetattr(uart0_filestream, &options);
	options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
	options.c_iflag = IGNPAR;
	options.c_oflag = 0;
	options.c_lflag = 0;

	tcflush(uart0_filestream, TCIFLUSH);
	tcsetattr(uart0_filestream, TCSANOW, &options);

	if( sucess_init_uart )
		{ uart_is_open = 1; sem_init(&mutex, 0, 1); }

	return sucess_init_uart;
}

void close_uart()
  { close(uart0_filestream); uart_is_open = 0; sem_destroy(&mutex); }

void add_crc_to_message()
{
  short int crc = calcula_CRC(tx_buffer, pointer_tx_buffer);
	memcpy( &tx_buffer[pointer_tx_buffer], &crc, sizeof(short) );
  pointer_tx_buffer += sizeof(short);
}

void add_matriculation()
{
	tx_buffer[pointer_tx_buffer++] = 3;
	tx_buffer[pointer_tx_buffer++] = 6;
	tx_buffer[pointer_tx_buffer++] = 2;
	tx_buffer[pointer_tx_buffer++] = 0;
}

int write_uart()
{
  int sucess_write_uart = 0;

  if (!uart_is_open)
		return sucess_write_uart;

	// printf("Escrevendo caracteres na UART : ");

	int count = write(uart0_filestream, &tx_buffer, pointer_tx_buffer);
	if (count <= 0)
		printf("UART TX error\n");
	else
	{
		// printf("%d elementos escritos.\n", count);
		sucess_write_uart = 1;
	}

	return sucess_write_uart;
}

int checked_crc_received()
{
  int size_of_message = (pointer_rx_buffer - sizeof(short));

  short crc_calculated = calcula_CRC(rx_buffer, size_of_message);

	short crc_received;
	memcpy(&crc_received, rx_buffer + size_of_message , sizeof(short));

	// printf("calculated_received: (%hd,%hd)\n", crc_calculated, crc_received);

	return (crc_calculated == crc_received);
}

int read_uart()
{
  usleep(50000);
  int sucess_read_uart = 0;

  if (!uart_is_open)
		return sucess_read_uart;

	pointer_rx_buffer = read(uart0_filestream, (void *)rx_buffer, 255);

	if (pointer_rx_buffer < 0)
		printf("Erro na leitura.\n");
	else if (pointer_rx_buffer == 0)
		printf("Nenhum dado disponível.\n");
	else if ( ! checked_crc_received() )
			printf("CRC ERROR!\n");
	else
		sucess_read_uart = 1;

	return sucess_read_uart;

}

int read_encoder_uart(int* encoder_value)
{
	int sucess_read_encoder_uart = 0;

	while(! init_uart()) sleep(1);
	sem_wait(&mutex);

	pointer_tx_buffer = 0;
	// ESP32 address.
	tx_buffer[pointer_tx_buffer++] = 0x01;

  // Add code, then sub-code
  tx_buffer[pointer_tx_buffer++] = 0x23;
  tx_buffer[pointer_tx_buffer++] = 0xC1;

  add_matriculation();
  add_crc_to_message();

  // Why if, then if ? To ensure write_uart and then try read_uart.
  if(write_uart()) if(read_uart())
  {
		memcpy(encoder_value, rx_buffer + 3, sizeof(int));
		sucess_read_encoder_uart = 1;
  }

  sem_post(&mutex);
  return sucess_read_encoder_uart;
}

int write_temperature_uart(char* temperature_string)
{
	int sucess_write_temperature_uart = 0;
	while(! init_uart()) sleep(1);
	sem_wait(&mutex);

	pointer_tx_buffer = 0;
	// ESP32 address.
	tx_buffer[pointer_tx_buffer++] = 0x01;

	// Add code, then sub-code
  tx_buffer[pointer_tx_buffer++] = 0x16;
  tx_buffer[pointer_tx_buffer++] = 0xD1;

  // Get formated temperature
  char temperature_string_formated[5];
  memcpy(temperature_string_formated, temperature_string, sizeof(char) * 4);
  float temperature = atof(temperature_string_formated);

  // Write temperature into buffer
  memcpy(tx_buffer + pointer_tx_buffer, &temperature, sizeof(float));
  pointer_tx_buffer += sizeof(float);

  add_matriculation();
  add_crc_to_message();

  if(write_uart()) if (read_uart())
  	sucess_write_temperature_uart = 1;

  sem_post(&mutex);
  return sucess_write_temperature_uart;
}

int write_pwm_uart(int* pwm_control_signal)
{
	int sucess_write_pwm_uart = 0;
	while(! init_uart()) sleep(1);
  sem_wait(&mutex);

	pointer_tx_buffer = 0;
	// ESP32 address.
	tx_buffer[pointer_tx_buffer++] = 0x01;

	// Add code, then sub-code
  tx_buffer[pointer_tx_buffer++] = 0x16;
  tx_buffer[pointer_tx_buffer++] = 0xC2;

  // Write pwm_control_signal into buffer
  memcpy(tx_buffer + pointer_tx_buffer, pwm_control_signal, sizeof(int));
  pointer_tx_buffer += sizeof(int);

  add_matriculation();
  add_crc_to_message();

  if(write_uart()) if (read_uart())
  	sucess_write_pwm_uart = 1;

  sem_post(&mutex);
	return sucess_write_pwm_uart;
}


int read_all_registers_uart(char* buttons_buffer, char max_elements)
{
	int sucess_read_all_registers_uart = 0;
	while(! init_uart()) sleep(1);
  sem_wait(&mutex);

	pointer_tx_buffer = 0;
	// ESP32 address.
	tx_buffer[pointer_tx_buffer++] = 0x01;

	// Add code, then base address.
  tx_buffer[pointer_tx_buffer++] = 0x03;
  tx_buffer[pointer_tx_buffer++] = 0x00;

  // Total elements to read.
  tx_buffer[pointer_tx_buffer++] = max_elements;

  add_matriculation();
  add_crc_to_message();

  if(write_uart()) if (read_uart())
  {
  		sucess_read_all_registers_uart = 1;
  		// Copy readed values to buffer.
  		memcpy( buttons_buffer, rx_buffer + 2, sizeof(char) * max_elements);
  }
  sem_post(&mutex);

	return sucess_read_all_registers_uart;
}

int write_all_registers_uart(char* buttons_buffer, char max_elements)
{
	int sucess_write_all_registers_uart = 0;
	while(! init_uart()) sleep(1);
  sem_wait(&mutex);

	pointer_tx_buffer = 0;
	// ESP32 address.
	tx_buffer[pointer_tx_buffer++] = 0x01;

	// Add code, then base address.
  tx_buffer[pointer_tx_buffer++] = 0x06;
  tx_buffer[pointer_tx_buffer++] = 0x00;

  // Total elements to write.
  tx_buffer[pointer_tx_buffer++] = max_elements;

	// Write elements.
  memcpy(tx_buffer + pointer_tx_buffer, buttons_buffer , sizeof(char) * max_elements);
  pointer_tx_buffer += sizeof(char) * max_elements;

  add_matriculation();
  add_crc_to_message();

  if(write_uart()) if (read_uart())
  	sucess_write_all_registers_uart = 1;

  sem_post(&mutex);
	return sucess_write_all_registers_uart;
}
